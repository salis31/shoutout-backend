var express = require('express');
var app = express();
var router = express.Router();

app.set('port', (process.env.PORT || 5000));

app.use('/posts', require('./api/post'))

app.get('/', function(req, res) {
  res.send('Hello world');
});

app.listen(app.get('port'), function() {
  console.log('is listening to port', app.get('port'));
});
